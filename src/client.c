#include "client.h"

char msg_counting_down[] = "cd";
char msg_return_to_menu[] = "em";
char msg_client_connected[] = "r1";
char msg_both_connected[] = "r2";
char msg_set_id_to_0[] = "i0";
char msg_set_id_to_1[] = "i1";
char msg_reset_actions[] = "i";
char msg_p1_win[] = "v1";
char msg_p2_win[] = "v2";
char msg_p1_punch[] = "p1";
char msg_p2_punch[] = "p2";
char msg_p1_block[] = "b1";
char msg_p2_block[] = "b2";
char msg_p1_charge[] = "c1";
char msg_p2_charge[] = "c2";
char msg_p1_dead[] = "d1";
char msg_p2_dead[] = "d2";
char msg_p1_heavy_hit[] = "h1";
char msg_p2_heavy_hit[] = "h2";
char msg_p1_light_hit[] = "l1";
char msg_p2_light_hit[] = "l2";
char msg_p1_miss[] = "m1";
char msg_p2_miss[] = "m2";
char msg_both_hit[] = "bh";

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void clientShutdown(int id)
{
	send(sockfd, "e1", 3, 0);
    client_running = 0;
    closesocket(id);
    WSACleanup();
    printf("Client %d shutting down\n", id);
}

thread_ret_t clientStart(void *f)
{
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);

    int numbytes;
    char buf[MAXDATASIZE];
    struct addrinfo hints, *servinfo, *p;
    int rv;
    char s[INET6_ADDRSTRLEN];

    client_running = 1;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	/*READ IP FROM serveraddress.txt*/
	FILE* fp;
	char ipbuffer[128];

	fp = fopen("serveraddress.txt", "r");

	while (fgets(ipbuffer, 128, (FILE*)fp)) {
		printf("%s\n", ipbuffer);
	}
	fclose(fp);
	/*******************************/

    char char_buffer[128];

    if ((rv = getaddrinfo(ipbuffer, PORT, &hints, &servinfo)) != 0)
    {
        sys_getSockErrorString(char_buffer, 128);
        fprintf(stderr, "getaddrinfo: %s\n", char_buffer);
        return 1;
    }

    // loop through all the results and connect to the first we can
    for (p = servinfo; p != NULL; p = p->ai_next)
    {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
            p->ai_protocol)) == -1) {
            sys_getSockErrorString(char_buffer, 128);
            fprintf(stderr, "socket: %s\n", char_buffer);
            continue;
        }

        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1)
        {
            closesocket(sockfd);
            sys_getSockErrorString(char_buffer, 128);
            fprintf(stderr, "connect: %s\n", char_buffer);
            continue;
        }

        break;
    }

    if (p == NULL)
    {
        fprintf(stderr, "Client: Failed to connect\n");
        return 2;
    }

    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
        s, sizeof s);
    printf("Client: Connecting to %s\n", s);

	ioctlsocket(sockfd, FIONBIO, 1);

    while (client_running)
    {
        if ((numbytes = recv(sockfd, buf, MAXDATASIZE - 1, 0)) == -1)
        {
            perror("recv");
        }
        else if (numbytes > 0)
        {
            buf[numbytes] = '\0';

            printf("Client: Received '%s'\n", buf);

			if (!strcmp(msg_counting_down, buf))
			{

				--client_session_ptr->countdown;
			}

			if (!strcmp(msg_return_to_menu, buf))
				client_session_ptr->gamestate = GAMESTATE_MENU;

			if (!strcmp(msg_client_connected, buf))
				client_session_ptr->gamestate = GAMESTATE_WAITING_OTHER_CLIENTS;

			if (!strcmp(msg_reset_actions, buf))
			{
				client_session_ptr->gamestate = GAMESTATE_ACTIVE;

				client_session_ptr->player[0].action_counter = 0;
				client_session_ptr->player[1].action_counter = 0;

				client_session_ptr->hpbar[0].player_actions[0] = 4;
				client_session_ptr->hpbar[0].player_actions[1] = 4;
				client_session_ptr->hpbar[0].player_actions[2] = 4;

				client_session_ptr->hpbar[1].player_actions[0] = 4;
				client_session_ptr->hpbar[1].player_actions[1] = 4;
				client_session_ptr->hpbar[1].player_actions[2] = 4;
			}

			if (!strcmp(msg_set_id_to_0, buf))
				player_id = 0;

			if (!strcmp(msg_set_id_to_1, buf))
				player_id = 1;

			if (!strcmp(msg_both_connected, buf))
				gamePlay(client_session_ptr);

			if (!strcmp(msg_p1_win, buf))
			{
				client_session_ptr->player[0].state = STATE_DEAD;
				client_session_ptr->player[0].hp = 0;
				textureSetClip(&client_session_ptr->hpbar[0].texture, 0, 0, 0, 0, 16);
				client_session_ptr->gamestate = GAMESTATE_GAMEOVER;
			}

			if (!strcmp(msg_p2_win, buf))
			{
				client_session_ptr->player[1].state = STATE_DEAD;
				client_session_ptr->player[1].hp = 0;
				textureSetClip(&client_session_ptr->hpbar[1].texture, 0, 0, 0, 0, 16);
				client_session_ptr->gamestate = GAMESTATE_GAMEOVER;
			}

			if (!strcmp(msg_p1_punch, buf))
			{
				playerSetState(client_session_ptr, &client_session_ptr->player[0], STATE_PUNCH);
			}

			if (!strcmp(msg_p2_punch, buf))
			{
				playerSetState(client_session_ptr, &client_session_ptr->player[1], STATE_PUNCH);
			}

			if (!strcmp(msg_p1_block, buf))
			{
				playerSetState(client_session_ptr, &client_session_ptr->player[0], STATE_BLOCK);
			}

			if (!strcmp(msg_p2_block, buf))
			{
				playerSetState(client_session_ptr, &client_session_ptr->player[1], STATE_BLOCK);
			}

			if (!strcmp(msg_p1_charge, buf))
			{
				playerSetState(client_session_ptr, &client_session_ptr->player[0], STATE_CHARGE);
				if (client_session_ptr->player[0].charge < 3)
					++client_session_ptr->player[0].charge;
			}

			if (!strcmp(msg_p2_charge, buf))
			{
				playerSetState(client_session_ptr, &client_session_ptr->player[1], STATE_CHARGE);
				if (client_session_ptr->player[1].charge < 3)
					++client_session_ptr->player[1].charge;
			}

			if (!strcmp(msg_p1_miss, buf))
			{
				client_session_ptr->player[0].charge = 0;
			}

			if (!strcmp(msg_p2_miss, buf))
			{
				client_session_ptr->player[1].charge = 0;
			}
			
			if (!strcmp(msg_p1_heavy_hit, buf))
			{
				playerTakeDamage(client_session_ptr, &client_session_ptr->player[0], 20 * (client_session_ptr->player[1].charge + 1), 2);
				client_session_ptr->player[1].charge = 0;
			}

			if (!strcmp(msg_p2_heavy_hit, buf))
			{
				playerTakeDamage(client_session_ptr, &client_session_ptr->player[1], 20 * (client_session_ptr->player[0].charge + 1), 2);
				client_session_ptr->player[0].charge = 0;
			}

			if (!strcmp(msg_both_hit, buf))
			{
				playerTakeDamage(client_session_ptr, &client_session_ptr->player[1], 20 * (client_session_ptr->player[0].charge + 1), 2);
				client_session_ptr->player[0].charge = 0;
				playerTakeDamage(client_session_ptr, &client_session_ptr->player[0], 20 * (client_session_ptr->player[1].charge + 1), 2);
				client_session_ptr->player[1].charge = 0;
			}

			if (!strcmp(msg_p1_light_hit, buf))
			{
				playerTakeDamage(client_session_ptr, &client_session_ptr->player[0], 20 * (client_session_ptr->player[1].charge + 1), 1);
				client_session_ptr->player[0].charge = 0;
				client_session_ptr->player[1].charge = 0;
			}

			if (!strcmp(msg_p2_light_hit, buf))
			{
				playerTakeDamage(client_session_ptr, &client_session_ptr->player[1], 20 * (client_session_ptr->player[0].charge + 1), 1);
				client_session_ptr->player[0].charge = 0;
				client_session_ptr->player[1].charge = 0;
			}

			if (!strcmp(msg_p1_dead, buf))
			{
				if (client_session_ptr->player[0].position_counter <= 0)
				{
					client_session_ptr->player[0].falling = 1;
				}
				client_session_ptr->player[0].state = STATE_DEAD;
				client_session_ptr->player[0].hp = 0;
				textureSetClip(&client_session_ptr->hpbar[0].texture, 0, 0, 0, 0, 16);
				client_session_ptr->gamestate = GAMESTATE_GAMEOVER;
			}

			if (!strcmp(msg_p2_dead, buf))
			{
				if (client_session_ptr->player[1].position_counter >= 20)
				{
					client_session_ptr->player[1].falling = 1;
				}
				client_session_ptr->player[1].state = STATE_DEAD;
				client_session_ptr->player[1].hp = 0;
				textureSetClip(&client_session_ptr->hpbar[1].texture, 0, 0, 0, 0, 16);
				client_session_ptr->gamestate = GAMESTATE_GAMEOVER;
			}
        }
    }
    return 0;
}

void clientSetSession(GameSession *gs)
{
	client_session_ptr = gs;
}