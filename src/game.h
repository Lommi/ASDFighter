#pragma once

#include "texture.h"
#include "sys_win32.h"
#include "client.h"

#define PLAYER1_DEFAULT_X_POSITION 190
#define PLAYER1_DEFAULT_Y_POSITION 116
#define PLAYER2_DEFAULT_X_POSITION 250
#define KNOCKBACK_DISTANCE 20
#define TIMER_TICK 6

enum GameState
{
	GAMESTATE_MENU = 1,
	GAMESTATE_EXECUTING,
	GAMESTATE_ACTIVE,
	GAMESTATE_WAITING_FOR_INPUT,
    GAMESTATE_GAMEOVER,
	GAMESTATE_CONNECTING_TO_SERVER,
	GAMESTATE_WAITING_OTHER_CLIENTS
};

enum PlayerState
{
	STATE_IDLE = 1,
	STATE_PUNCH,
	STATE_BLOCK,
	STATE_CHARGE,
	STATE_HURT,
    STATE_DEAD
};

enum EffectTypes
{
    EFFECT_HIT_SMALL = 1,
    EFFECT_HIT_LARGE,
    EFFECT_BLOCK
};

typedef struct Player		Player;
typedef struct Effect       Effect;
typedef struct Healthbar    Healthbar;
typedef struct GameSession	GameSession;

struct Player
{
	int		state;
	int		current_frame;
	int		x, y;
    int     punch_counter;
    int     hp;
	int		position_counter;
	int		hit_dir, block_dir;
	int		action_counter;
	int		charge;
    bool    falling;
	Texture texture;
	Healthbar *hpbar;
};

struct Effect
{
    int     x, y;
    int     frame_count;
    int     current_frame;
    int     visible;
    Texture texture;
};

struct Healthbar
{
    int     x, y;
    Texture texture;
	Texture action_icons_texture;
	int		player_actions[3];
};

struct GameSession
{
    int         countdown;
    int         countdown_timer;
	int		    gamestate;
	int		    update_frames;
	Texture     texture_player;
	Texture     texture_titles;
	Texture		texture_gameover_text;
    Texture     texture_hp_border;
    Texture     texture_hp_fill;
    Texture     texture_background;
    Texture     texture_border;
	Texture		texture_actions;
	Player      player[2];
	Healthbar   hpbar[2];
    Effect      effect;
};

Thread client_thread;
int socketid;
extern int player_id;

bool d_pressed, s_pressed, a_pressed;

void gameInit(GameSession *session, SDL_Renderer *renderer);
void gamePlay(GameSession *session);
void gameUpdate(GameSession *session);
void gameRender(GameSession *session, SDL_Renderer* renderer);

void playerSetState(GameSession *session, Player *player, int state);
void playerUpdate(GameSession *session, Player *player);
void playerTakeDamage(GameSession *session, Player *player, int amount, int position_amount);

void effectShow(GameSession *session, int type, int x, int y);
void effectUpdate(GameSession *session);