#include "server.h"

void sql_check()
{
	//Client 1 wins
	query = sqlite3_mprintf("UPDATE players SET losses = losses + 1 WHERE ip=('%q');", client1_ip);
	rc = sqlite3_exec(db, query, callback, 0, &errmsg);
	sqlite3_free(query);
	query = sqlite3_mprintf("UPDATE players SET wins = wins + 1 WHERE ip=('%q');", client2_ip);
	rc = sqlite3_exec(db, query, callback, 0, &errmsg);
	sqlite3_free(query);
	/*Check if we have a new champion*/
	query = sqlite3_mprintf("SELECT ip FROM championtable WHERE ip=('%q');", client1_ip);
	int result = sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
	sqlite3_free(query);
	if (result == SQLITE_OK)
	{
		if (sqlite3_step(stmt) == SQLITE_ROW)
		{
			printf("Client 2 is the new champion champion!\n");

			query = sqlite3_mprintf("UPDATE championtable SET ip = ('%q') WHERE ip=('%q');", client2_ip, client1_ip);

			rc = sqlite3_exec(db, query, callback, 0, &errmsg);
			if (rc != SQLITE_OK) {
				fprintf(stderr, "SQL error: %s\n", errmsg);
				sqlite3_free(errmsg);
			}
		}
	}
	sqlite3_finalize(stmt);
	sqlite3_free(query);

	//Client 2 wins
	query = sqlite3_mprintf("UPDATE players SET winstreak = 0 WHERE ip=('%q');", client1_ip);
	rc = sqlite3_exec(db, query, callback, 0, &errmsg);
	sqlite3_free(query);
	query = sqlite3_mprintf("UPDATE players SET winstreak = winstreak + 1 WHERE ip=('%q');", client2_ip);
	rc = sqlite3_exec(db, query, callback, 0, &errmsg);
	sqlite3_free(query);
	/*Check if we have a new champion*/
	query = sqlite3_mprintf("SELECT ip FROM championtable WHERE ip=('%q');", client2_ip);
	result = sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
	sqlite3_free(query);
	if (result == SQLITE_OK)
	{
		if (sqlite3_step(stmt) == SQLITE_ROW)
		{
			printf("Client 1 is the new champion champion!\n");

			query = sqlite3_mprintf("UPDATE championtable SET ip = ('%q') WHERE ip=('%q');", client1_ip, client2_ip);

			rc = sqlite3_exec(db, query, callback, 0, &errmsg);
			if (rc != SQLITE_OK) {
				fprintf(stderr, "SQL error: %s\n", errmsg);
				sqlite3_free(errmsg);
			}
		}
	}
	sqlite3_finalize(stmt);
	sqlite3_free(query);
}

static int callback(void *data, int argc, char **argv, char **azColName)
{
	int i;
	fprintf(stderr, "%s: ", (const char*)data);
	for (i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "");
	}
	printf("\n");

	return 0;
}

int ban_player(char *address)
{
	query = sqlite3_mprintf("insert into bantable (ip) values ('%q');", address);

	rc = sqlite3_exec(db, query, callback, address, &errmsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", errmsg);
		sqlite3_free(errmsg);
	}
	else {
		fprintf(stdout, "Ban insert was successfull\n");
	}
	sqlite3_free(query);
	return 0;
}

// get sockaddr, IPv4 or IPv6:
void *get_server_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void serverShutdown()
{
    //if (send(accepted_clients[1], "rr", 3, 0) == -1) //shutdown other client
    //    perror("send");

	client_count = 0;
	server_running = 0;
	acceptloop_running = 0;
	gamestate = GAMESTATE_NONE;

    WSACleanup();
    printf("Server shutting down\n");
}

void serverNeutralize()
{
	client_count = 0;
	acceptloop_running = 1;
	gamestate = GAMESTATE_NONE;
	replay_count = 0;
	printf("Server: No clients connected. Return to acceptloop\n");
}

int main(int argc, char* args[])
{
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);

    int sockfd;  // listen on sock_fd, new connection on new_fd
    struct addrinfo hints, *servinfo, *p;
    struct sockaddr_storage their_addr; // connector's address information
    socklen_t sin_size;
    int yes = 1;

    char s[INET6_ADDRSTRLEN];
    int rv;

    client_count = 0;
	server_running = 1;
	gamestate = GAMESTATE_NONE;
	acceptloop_running = 0;
	replay_count = 0;

    memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	
	//SQL
	rc = sqlite3_open("playerdatabase", &db);
	if (rc)
		printf("can't open database\n");
	else
		printf("database opened\n");
	//playerlist
	rc = sqlite3_exec(db, "CREATE TABLE players(ip TEXT, logins INTEGER, wins INTEGER, losses INTEGER, winstreak INTEGER, UNIQUE(ip));", callback, 0, &errmsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", errmsg);
		sqlite3_free(errmsg);
	}
	else {
		fprintf(stdout, "Playertable created successfully\n");
	}
	//banlist
	rc = sqlite3_exec(db, "CREATE TABLE bantable(ip TEXT, UNIQUE(ip));", callback, 0, &errmsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", errmsg);
		sqlite3_free(errmsg);
	}
	else {
		fprintf(stdout, "Bantable created successfully\n");
	}
	//championlist
	rc = sqlite3_exec(db, "CREATE TABLE championtable(ip TEXT, UNIQUE(ip));", callback, 0, &errmsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", errmsg);
		sqlite3_free(errmsg);
	}
	else {
		fprintf(stdout, "Championtable created successfully\n");
	}
	//

    if ((rv = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0)
    {
        fprintf(stdout, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

    // loop through all the results and bind to the first we can
    for (p = servinfo; p != NULL; p = p->ai_next)
    {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
            p->ai_protocol)) == -1)
        {
            perror("Server: Socket");
            continue;
        }

        if (setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (char*)&yes,
            sizeof(int)) == -1)
        {
            perror("setsockopt");
            exit(1);
        }

        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1)
        {
            closesocket(sockfd);
            perror("Server: Bind");
            continue;
        }

        break;
    }

    freeaddrinfo(servinfo); // all done with this structure

    if (p == NULL)
    {
        fprintf(stderr, "Server: Failed to bind\n");
        exit(1);
    }

    if (listen(sockfd, BACKLOG) == -1)
    {
        perror("listen");
        exit(1);
    }

	// Put the socket in non-blocking mode: WINDOWS
	//ioctlsocket(sockfd, FIONBIO, 0);

	acceptloop_running = 1;
    printf("Server: Waiting for connections...\n");

	while (server_running)
	{
		while (acceptloop_running)
		{
			printf("Server: Main accept loop running\n");
			printf("Server: Client count %d\n", client_count);
			sin_size = sizeof their_addr;
			accepted_clients[client_count] = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);

			if (accepted_clients[client_count] == -1)
			{
				perror("accept");
				continue;
			}

			inet_ntop(their_addr.ss_family,
				get_server_addr((struct sockaddr *)&their_addr),
				s, sizeof s);
			printf("Server: Got connection from %s\n", s);

			strcpy(sql_string, s);
			/*Check if ip is banned*/
			query = sqlite3_mprintf("SELECT ip FROM bantable WHERE ip=('%q');", sql_string);
			int result = sqlite3_prepare_v2(db, query, -1, &stmt, NULL);
			if (result == SQLITE_OK)
			{
				if (sqlite3_step(stmt) == SQLITE_ROW)
				{
					printf("Address is banned. Refusing connection!\n");
					continue;
				}
			}
			sqlite3_finalize(stmt);
			sqlite3_free(query);

			/*insert new entry to table*/
			query = sqlite3_mprintf("insert into players (ip, logins, wins, losses, winstreak) values ('%q', 0, 0, 0, 0);", sql_string);

			rc = sqlite3_exec(db, query, callback, sql_string, &errmsg);
			if (rc != SQLITE_OK) {
				fprintf(stderr, "SQL error: %s\n", errmsg);
				sqlite3_free(errmsg);
			}
			sqlite3_free(query);

			/*insert default champion
			query = sqlite3_mprintf("insert into championtable (ip) values ('%q');", sql_string);

			rc = sqlite3_exec(db, query, callback, sql_string, &errmsg);
			if (rc != SQLITE_OK) {
				fprintf(stderr, "SQL error: %s\n", errmsg);
				sqlite3_free(errmsg);
			}
			sqlite3_free(query);*/

			/*update new values to ip*/
			query = sqlite3_mprintf("UPDATE players SET logins = logins + 1 WHERE ip=('%q');", sql_string);

			rc = sqlite3_exec(db, query, callback, 0, &errmsg);
			if (rc != SQLITE_OK) {
				fprintf(stderr, "SQL error: %s\n", errmsg);
				sqlite3_free(errmsg);
			}
			sqlite3_free(query);

			/*select login count from ip*/
			query = sqlite3_mprintf("SELECT * FROM players WHERE ip=('%q');", sql_string);
			rc = sqlite3_exec(db, query, callback, 0, &errmsg);
			if (rc != SQLITE_OK) {
				fprintf(stderr, "SQL error: %s\n", errmsg);
				sqlite3_free(errmsg);
			}
			sqlite3_free(query);

			/*select champion from championtable*/
			printf("Current champion:\n");
			rc = sqlite3_exec(db, "SELECT * FROM championtable;", callback, 0, &errmsg);
			if (rc != SQLITE_OK) {
				fprintf(stderr, "SQL error: %s\n", errmsg);
				sqlite3_free(errmsg);
			}

			if (client_count == 0)
			{
				++client_count;

				send(accepted_clients[0], "r1", 3, 0);
				send(accepted_clients[0], "i0", 3, 0);
				printf("Server: Sent confirmation message to client 1\n");
				strcpy(client1_ip, s);

				Thread_create(&client1_listener, serverHearClient1, 0);
			}
			else if (client_count == 1)
			{
				++client_count;

				printf("Server: Client count %d\n", client_count);
				printf("Server: Both clients connected!\n");

				acceptloop_running = 0;
				
				matchReset();

				send(accepted_clients[0], "r2", 3, 0);
				send(accepted_clients[1], "r2", 3, 0);
				send(accepted_clients[1], "i1", 3, 0);
				printf("Server: Sent ready message to both clients\n");
				printf("	Session started! Listening to clients..\n");
				strcpy(client2_ip, s);

				Thread_create(&client2_listener, serverHearClient2, 0);
			}
			else
			{
				printf("Server: Server full! Two clients have already connected!\n");
			}
		}

		while (gamestate == GAMESTATE_COUNTDOWN)
		{
			if (countdown > 0)
			{
				Sleep(1000);
				printf("Match starting in %d \n", countdown);

				--countdown;

				send(accepted_clients[0], "cd", 3, 0);
				send(accepted_clients[1], "cd", 3, 0);
			}
			else
				gamestate = GAMESTATE_TAKING_INPUT;
		}

		while(gamestate == GAMESTATE_GAMEOVER)
		{
			if (replay_count == 2)
			{
				matchReset();

				send(accepted_clients[0], "r2", 3, 0);
				send(accepted_clients[1], "r2", 3, 0);

				printf("Server: Starting countdown!\n");
			}
		}

		while (gamestate == GAMESTATE_EXECUTING)
		{
			if (countdown > 0)
			{
				Sleep(300);
				printf("Executing action %d \n", countdown);

				--countdown;

				switch (p1_actions[p1_action_counter])
				{
				case ACTION_PUNCH:
					send(accepted_clients[0], "p1", 3, 0);
					send(accepted_clients[1], "p1", 3, 0);
					p1_position += 1;
					break;
				case ACTION_BLOCK:
					send(accepted_clients[0], "b1", 3, 0);
					send(accepted_clients[1], "b1", 3, 0);
					p1_position -= 1;
					break;
				case ACTION_CHARGE:
					send(accepted_clients[0], "c1", 3, 0);
					send(accepted_clients[1], "c1", 3, 0);
					if(p1_charge < 3)
						++p1_charge;
					break;
				}

				switch (p2_actions[p2_action_counter])
				{
				case ACTION_PUNCH:
					send(accepted_clients[0], "p2", 3, 0);
					send(accepted_clients[1], "p2", 3, 0);
					p2_position -= 1;
					break;
				case ACTION_BLOCK:
					send(accepted_clients[0], "b2", 3, 0);
					send(accepted_clients[1], "b2", 3, 0);
					p2_position += 1;
					break;
				case ACTION_CHARGE:
					send(accepted_clients[0], "c2", 3, 0);
					send(accepted_clients[1], "c2", 3, 0);
					if (p2_charge < 3)
						++p2_charge;
					break;
				}

				if ((p1_actions[p1_action_counter] == ACTION_PUNCH && p2_actions[p2_action_counter] == ACTION_PUNCH)
					&& ((p1_position == p2_position)
						|| (p1_position == p2_position + 1)
						|| (p1_position == p2_position - 1)))
				{
					if (p1_charge == p2_charge)
					{
						p1_hp -= 20 * (p2_charge + 1);
						p2_hp -= 20 * (p1_charge + 1);
						p1_position -= 2;
						p2_position += 2;

						send(accepted_clients[0], "bh", 3, 0);
						send(accepted_clients[1], "bh", 3, 0);
					}
					else if (p1_charge > p2_charge)
					{
						p2_hp -= 20 * (p1_charge + 1);
						p2_position += 2;

						send(accepted_clients[0], "h2", 3, 0);
						send(accepted_clients[1], "h2", 3, 0);
					}
					else if (p1_charge < p2_charge)
					{
						p1_hp -= 20 * (p2_charge + 1);
						p1_position -= 2;

						send(accepted_clients[0], "h1", 3, 0);
						send(accepted_clients[1], "h1", 3, 0);
					}
				}
				else if ((p1_actions[p1_action_counter] == ACTION_PUNCH && p2_actions[p2_action_counter] == ACTION_CHARGE)
					&& (p1_position == p2_position - 1))
				{
					p2_hp -= 20 * (p1_charge + 1);
					p2_position += 1;
					p2_charge = 0;

					send(accepted_clients[0], "l2", 3, 0);
					send(accepted_clients[1], "l2", 3, 0);
				}
				else if ((p1_actions[p1_action_counter] == ACTION_CHARGE && p2_actions[p2_action_counter] == ACTION_PUNCH)
					&& (p1_position == p2_position - 1))
				{
					p1_hp -= 20 * (p2_charge + 1);
					p1_position -= 1;
					p1_charge = 0;

					send(accepted_clients[0], "l1", 3, 0);
					send(accepted_clients[1], "l1", 3, 0);
				}

				if (p1_actions[p1_action_counter] == ACTION_PUNCH)
				{
					p1_charge = 0;
					send(accepted_clients[0], "m1", 3, 0);
					send(accepted_clients[1], "m1", 3, 0);
				}

				if (p2_actions[p2_action_counter] == ACTION_PUNCH)
				{
					p2_charge = 0;
					send(accepted_clients[0], "m2", 3, 0);
					send(accepted_clients[1], "m2", 3, 0);
				}

				++p1_action_counter;
				++p2_action_counter;

				checkPlayerStatus();
			}
			else
			{
				Sleep(300);
				gamestate = GAMESTATE_TAKING_INPUT;
				p1_action_counter = 0;
				p2_action_counter = 0;
				p1_ready = 0;
				p2_ready = 0;
				send(accepted_clients[0], "i", 3, 0);
				send(accepted_clients[1], "i", 3, 0);
				printf("Server: Execution complete. New round begins!\n");
			}
		}
	}

	sqlite3_close(db);
    return 0;
}

thread_ret_t serverHearClient1(void *f)
{
	while (server_running)
	{
		msg1 = recv(accepted_clients[0], buf1, MAXDATASIZE - 1, 0);

		if (msg1 > 0)
		{
			buf1[msg1] = '\0';

			if (!strcmp("e1", buf1))
			{
				--client_count;

				send(accepted_clients[0], "v2", 3, 0);
				send(accepted_clients[1], "v2", 3, 0);
				replay_count = 0;
				gamestate = GAMESTATE_GAMEOVER;
				printf("Server: Client 1 disconnected\n");

				if (client_count == 0)
					serverNeutralize();
				else if(client_count == 1)
					sql_check();

				Thread_join(&client1_listener);
			}

			if (gamestate == GAMESTATE_TAKING_INPUT)
			{
				if (p1_action_counter < 3)
				{
					if (!strcmp("p", buf1))
					{
						printf("Server: P1 PUNCH\n", buf1);
						p1_actions[p1_action_counter] = ACTION_PUNCH;
						++p1_action_counter;
					}

					if (!strcmp("b", buf1))
					{
						printf("Server: P1 BLOCK\n", buf1);
						p1_actions[p1_action_counter] = ACTION_BLOCK;
						++p1_action_counter;
					}

					if (!strcmp("c", buf1))
					{
						printf("Server: P1 CHARGE\n", buf1);
						p1_actions[p1_action_counter] = ACTION_CHARGE;
						++p1_action_counter;
					}

					if (p1_action_counter >= 3)
					{
						p1_ready = 1;
						printf("Server: P1 READY\n", buf1);
						if (p2_ready)
						{
							printf("Server: BOTH READY. EXECUTING ACTIONS!\n", buf1);
							gamestate = GAMESTATE_EXECUTING;
							countdown = 3;
							p1_action_counter = 0;
							p2_action_counter = 0;
							p1_ready = 0;
							p2_ready = 0;
						}
					}
				}
			}

			if (gamestate == GAMESTATE_GAMEOVER)
			{
				if (!strcmp("r", buf1))
				{
					if (client_count == 2)
					{
						++replay_count;
						printf("Server: Player 1 wants to play again\n");
					}
					else
					{
						send(accepted_clients[0], "em", 3, 0);
						serverNeutralize();
					}
				}
			}

			buf1[msg1] = 0;
		}
	}

	printf("Server: Stopped listening Client 1\n");
	Thread_join(&client1_listener);

	return 0;
}

thread_ret_t serverHearClient2(void *f)
{
	while (server_running)
	{
		msg2 = recv(accepted_clients[1], buf2, MAXDATASIZE - 1, 0);

		if (msg2 > 0)
		{
			buf2[msg2] = '\0';

			if (!strcmp("e1", buf2))
			{
				--client_count;

				send(accepted_clients[0], "v1", 3, 0);
				send(accepted_clients[1], "v1", 3, 0);
				replay_count = 0;
				gamestate = GAMESTATE_GAMEOVER;
				printf("Server: Client 2 disconnected\n");

				if (client_count == 0)
					serverNeutralize();
				else if (client_count == 1)
					sql_check();

				Thread_join(&client2_listener);
			}

			if (gamestate == GAMESTATE_TAKING_INPUT)
			{
				if (p2_action_counter < 3)
				{
					if (!strcmp("p", buf2))
					{
						printf("Server: P2 PUNCH\n", buf2);
						p2_actions[p2_action_counter] = ACTION_PUNCH;
						++p2_action_counter;
					}

					if (!strcmp("b", buf2))
					{
						printf("Server: P2 BLOCK\n", buf2);
						p2_actions[p2_action_counter] = ACTION_BLOCK;
						++p2_action_counter;
					}

					if (!strcmp("c", buf2))
					{
						printf("Server: P2 CHARGE\n", buf2);
						p2_actions[p2_action_counter] = ACTION_CHARGE;
						++p2_action_counter;
					}

					if (p2_action_counter >= 3)
					{
						p2_ready = 1;
						printf("Server: P2 READY\n", buf2);
						if (p1_ready)
						{
							printf("Server: BOTH READY. EXECUTING ACTIONS!\n", buf2);
							gamestate = GAMESTATE_EXECUTING;
							countdown = 3;
							p1_action_counter = 0;
							p2_action_counter = 0;
							p1_ready = 0;
							p2_ready = 0;
						}
					}
				}
			}

			if (gamestate == GAMESTATE_GAMEOVER)
			{
				if (!strcmp("r", buf2))
				{
					if (client_count == 2)
					{
						++replay_count;
						printf("Server: Player 2 wants to play again\n");
					}
					else
					{
						send(accepted_clients[1], "em", 3, 0);
						serverNeutralize();
					}
				}
			}

			buf2[msg2] = 0;
		}
	}

	printf("Server: Stopped listening Client 2\n");
	Thread_join(&client2_listener);

	return 0;
}

void checkPlayerStatus()
{
	if (p1_position <= 0 || p2_position >= 20 || p1_hp <= 0 || p2_hp <= 0)
	{
		gamestate = GAMESTATE_GAMEOVER;
		printf("Server: GAME OVER!\n");
	}

	if (p1_position <= 0 || p1_hp <= 0)
	{
		send(accepted_clients[0], "d1", 3, 0);
		send(accepted_clients[1], "d1", 3, 0);
		printf("Server: P1 DEAD!\n");

		sql_check();
	}

	if (p2_position >= 20 || p2_hp <= 0)
	{
		send(accepted_clients[0], "d2", 3, 0);
		send(accepted_clients[1], "d2", 3, 0);
		printf("Server: P2 DEAD!\n");

		sql_check();
	}


}

void matchReset()
{
	replay_count = 0;

	countdown = 3;
	gamestate = GAMESTATE_COUNTDOWN;

	p1_ready = 0;
	p2_ready = 0;

	p1_charge = 0;
	p2_charge = 0;

	p1_position = 8;
	p2_position = 12;

	p1_hp = 100;
	p2_hp = 100;

	p1_action_counter = 0;
	p2_action_counter = 0;

	for (int i = 0; i < 3; ++i)
	{
		p1_actions[i] = ACTION_PUNCH;
		p2_actions[i] = ACTION_PUNCH;
	}
}