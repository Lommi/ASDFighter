#include "unix_server.h"

// get sockaddr, IPv4 or IPv6:
void *get_server_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void serverShutdown()
{
    //if (send(accepted_clients[1], "rr", 3, 0) == -1) //shutdown other client
    //    perror("send");

	client_count = 0;
	server_running = 0;
	acceptloop_running = 0;
	gamestate = GAMESTATE_NONE;

    printf("Server shutting down\n");
}

void serverNeutralize()
{
	client_count = 0;
	acceptloop_running = 1;
	gamestate = GAMESTATE_NONE;
	replay_count = 0;
	printf("Server: No clients connected. Return to acceptloop\n");
}

int main(int argc, char* args[])
{
    int sockfd;  // listen on sock_fd, new connection on new_fd
    struct addrinfo hints, *servinfo, *p;
    struct sockaddr_storage their_addr; // connector's address information
    socklen_t sin_size;
    int yes = 1;

    char s[INET6_ADDRSTRLEN];
    int rv;

    client_count = 0;
	server_running = 1;
	gamestate = GAMESTATE_NONE;
	acceptloop_running = 0;
	replay_count = 0;

    memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

    if ((rv = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0)
    {
        fprintf(stdout, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

    // loop through all the results and bind to the first we can
    for (p = servinfo; p != NULL; p = p->ai_next)
    {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
            p->ai_protocol)) == -1)
        {
            perror("Server: Socket");
            continue;
        }

        if (setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (char*)&yes,
            sizeof(int)) == -1)
        {
            perror("setsockopt");
            exit(1);
        }

        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1)
        {
            close(sockfd);
            perror("Server: Bind");
            continue;
        }

        break;
    }

    freeaddrinfo(servinfo); // all done with this structure

    if (p == NULL)
    {
        fprintf(stderr, "Server: Failed to bind\n");
        exit(1);
    }

    if (listen(sockfd, BACKLOG) == -1)
    {
        perror("listen");
        exit(1);
    }

	// Put the socket in non-blocking mode: WINDOWS
	//ioctlsocket(sockfd, FIONBIO, 0);

	acceptloop_running = 1;
    printf("Server: Waiting for connections...\n");

	while (server_running)
	{
		while (acceptloop_running)
		{
			printf("Server: Main accept loop running\n");
			printf("Server: Client count %d\n", client_count);
			sin_size = sizeof their_addr;
			accepted_clients[client_count] = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);

			if (accepted_clients[client_count] == -1)
			{
				perror("accept");
				continue;
			}

			inet_ntop(their_addr.ss_family,
				get_server_addr((struct sockaddr *)&their_addr),
				s, sizeof s);
			printf("Server: Got connection from %s\n", s);

			if (client_count == 0)
			{
				++client_count;

				send(accepted_clients[0], "r1", 3, 0);
				send(accepted_clients[0], "i0", 3, 0);
				printf("Server: Sent confirmation message to client 1\n");

				Thread_create(&client1_listener, serverHearClient1, 0);
			}
			else if (client_count == 1)
			{
				++client_count;

				printf("Server: Client count %d\n", client_count);
				printf("Server: Both clients connected!\n");

				acceptloop_running = 0;
				
				matchReset();

				send(accepted_clients[0], "r2", 3, 0);
				send(accepted_clients[1], "r2", 3, 0);
				send(accepted_clients[1], "i1", 3, 0);
				printf("Server: Sent ready message to both clients\n");
				printf("	Session started! Listening to clients..\n");

				Thread_create(&client2_listener, serverHearClient2, 0);
			}
			else
			{
				printf("Server: Server full! Two clients have already connected!\n");
			}
		}

		while (gamestate == GAMESTATE_COUNTDOWN)
		{
			if (countdown > 0)
			{
				sys_sleepMilliseconds(1000);
				printf("Match starting in %d \n", countdown);

				--countdown;

				send(accepted_clients[0], "cd", 3, 0);
				send(accepted_clients[1], "cd", 3, 0);
			}
			else
				gamestate = GAMESTATE_TAKING_INPUT;
		}

		while(gamestate == GAMESTATE_GAMEOVER)
		{
			if (replay_count == 2)
			{
				matchReset();

				send(accepted_clients[0], "r2", 3, 0);
				send(accepted_clients[1], "r2", 3, 0);

				printf("Server: Starting countdown!\n");
			}
		}

		while (gamestate == GAMESTATE_EXECUTING)
		{
			if (countdown > 0)
			{
				sys_sleepMilliseconds(300);
				printf("Executing action %d \n", countdown);

				--countdown;

				switch (p1_actions[p1_action_counter])
				{
				case ACTION_PUNCH:
					send(accepted_clients[0], "p1", 3, 0);
					send(accepted_clients[1], "p1", 3, 0);
					p1_position += 1;
					break;
				case ACTION_BLOCK:
					send(accepted_clients[0], "b1", 3, 0);
					send(accepted_clients[1], "b1", 3, 0);
					p1_position -= 1;
					break;
				case ACTION_CHARGE:
					send(accepted_clients[0], "c1", 3, 0);
					send(accepted_clients[1], "c1", 3, 0);
					if(p1_charge < 3)
						++p1_charge;
					break;
				}

				switch (p2_actions[p2_action_counter])
				{
				case ACTION_PUNCH:
					send(accepted_clients[0], "p2", 3, 0);
					send(accepted_clients[1], "p2", 3, 0);
					p2_position -= 1;
					break;
				case ACTION_BLOCK:
					send(accepted_clients[0], "b2", 3, 0);
					send(accepted_clients[1], "b2", 3, 0);
					p2_position += 1;
					break;
				case ACTION_CHARGE:
					send(accepted_clients[0], "c2", 3, 0);
					send(accepted_clients[1], "c2", 3, 0);
					if (p2_charge < 3)
						++p2_charge;
					break;
				}

				if ((p1_actions[p1_action_counter] == ACTION_PUNCH && p2_actions[p2_action_counter] == ACTION_PUNCH)
					&& ((p1_position == p2_position)
						|| (p1_position == p2_position + 1)
						|| (p1_position == p2_position - 1)))
				{
					if (p1_charge == p2_charge)
					{
						p1_hp -= 20 * (p2_charge + 1);
						p2_hp -= 20 * (p1_charge + 1);
						p1_position -= 2;
						p2_position += 2;

						send(accepted_clients[0], "bh", 3, 0);
						send(accepted_clients[1], "bh", 3, 0);
					}
					else if (p1_charge > p2_charge)
					{
						p2_hp -= 20 * (p1_charge + 1);
						p2_position += 2;

						send(accepted_clients[0], "h2", 3, 0);
						send(accepted_clients[1], "h2", 3, 0);
					}
					else if (p1_charge < p2_charge)
					{
						p1_hp -= 20 * (p2_charge + 1);
						p1_position -= 2;

						send(accepted_clients[0], "h1", 3, 0);
						send(accepted_clients[1], "h1", 3, 0);
					}
				}
				else if ((p1_actions[p1_action_counter] == ACTION_PUNCH && p2_actions[p2_action_counter] == ACTION_CHARGE)
					&& (p1_position == p2_position - 1))
				{
					p2_hp -= 20 * (p1_charge + 1);
					p2_position += 1;
					p2_charge = 0;

					send(accepted_clients[0], "l2", 3, 0);
					send(accepted_clients[1], "l2", 3, 0);
				}
				else if ((p1_actions[p1_action_counter] == ACTION_CHARGE && p2_actions[p2_action_counter] == ACTION_PUNCH)
					&& (p1_position == p2_position - 1))
				{
					p1_hp -= 20 * (p2_charge + 1);
					p1_position -= 1;
					p1_charge = 0;

					send(accepted_clients[0], "l1", 3, 0);
					send(accepted_clients[1], "l1", 3, 0);
				}

				if (p1_actions[p1_action_counter] == ACTION_PUNCH)
				{
					p1_charge = 0;
					send(accepted_clients[0], "m1", 3, 0);
					send(accepted_clients[1], "m1", 3, 0);
				}

				if (p2_actions[p2_action_counter] == ACTION_PUNCH)
				{
					p2_charge = 0;
					send(accepted_clients[0], "m2", 3, 0);
					send(accepted_clients[1], "m2", 3, 0);
				}

				++p1_action_counter;
				++p2_action_counter;

				checkPlayerStatus();
			}
			else
			{
				sys_sleepMilliseconds(300);
				gamestate = GAMESTATE_TAKING_INPUT;
				p1_action_counter = 0;
				p2_action_counter = 0;
				p1_ready = 0;
				p2_ready = 0;
				send(accepted_clients[0], "i", 3, 0);
				send(accepted_clients[1], "i", 3, 0);
				printf("Server: Execution complete. New round begins!\n");
			}
		}
	}
    return 0;
}

thread_ret_t serverHearClient1(void *f)
{
	while (server_running)
	{
		msg1 = recv(accepted_clients[0], buf1, MAXDATASIZE - 1, 0);

		if (msg1 > 0)
		{
			buf1[msg1] = '\0';

			if (!strcmp("e1", buf1))
			{
				--client_count;

				send(accepted_clients[0], "v2", 3, 0);
				send(accepted_clients[1], "v2", 3, 0);
				replay_count = 0;
				gamestate = GAMESTATE_GAMEOVER;
				printf("Server: Client 1 disconnected\n");

				if (client_count == 0)
				{
					serverNeutralize();
				}

				Thread_join(&client1_listener);
			}

			if (gamestate == GAMESTATE_TAKING_INPUT)
			{
				if (p1_action_counter < 3)
				{
					if (!strcmp("p", buf1))
					{
						printf("Server: P1 PUNCH\n", buf1);
						p1_actions[p1_action_counter] = ACTION_PUNCH;
						++p1_action_counter;
					}

					if (!strcmp("b", buf1))
					{
						printf("Server: P1 BLOCK\n", buf1);
						p1_actions[p1_action_counter] = ACTION_BLOCK;
						++p1_action_counter;
					}

					if (!strcmp("c", buf1))
					{
						printf("Server: P1 CHARGE\n", buf1);
						p1_actions[p1_action_counter] = ACTION_CHARGE;
						++p1_action_counter;
					}

					if (p1_action_counter >= 3)
					{
						p1_ready = 1;
						printf("Server: P1 READY\n", buf1);
						if (p2_ready)
						{
							printf("Server: BOTH READY. EXECUTING ACTIONS!\n", buf1);
							gamestate = GAMESTATE_EXECUTING;
							countdown = 3;
							p1_action_counter = 0;
							p2_action_counter = 0;
							p1_ready = 0;
							p2_ready = 0;
						}
					}
				}
			}

			if (gamestate == GAMESTATE_GAMEOVER)
			{
				if (!strcmp("r", buf1))
				{
					if (client_count == 2)
					{
						++replay_count;
						printf("Server: Player 1 wants to play again\n");
					}
					else
					{
						send(accepted_clients[0], "em", 3, 0);
						serverNeutralize();
					}
				}
			}

			buf1[msg1] = 0;
		}
	}

	printf("Server: Stopped listening Client 1\n");
	Thread_join(&client1_listener);

	return 0;
}

thread_ret_t serverHearClient2(void *f)
{
	while (server_running)
	{
		msg2 = recv(accepted_clients[1], buf2, MAXDATASIZE - 1, 0);

		if (msg2 > 0)
		{
			buf2[msg2] = '\0';

			if (!strcmp("e1", buf2))
			{
				--client_count;

				send(accepted_clients[0], "v1", 3, 0);
				send(accepted_clients[1], "v1", 3, 0);
				replay_count = 0;
				gamestate = GAMESTATE_GAMEOVER;
				printf("Server: Client 2 disconnected\n");

				if (client_count == 0)
				{
					serverNeutralize();
				}

				Thread_join(&client2_listener);
			}

			if (gamestate == GAMESTATE_TAKING_INPUT)
			{
				if (p2_action_counter < 3)
				{
					if (!strcmp("p", buf2))
					{
						printf("Server: P2 PUNCH\n", buf2);
						p2_actions[p2_action_counter] = ACTION_PUNCH;
						++p2_action_counter;
					}

					if (!strcmp("b", buf2))
					{
						printf("Server: P2 BLOCK\n", buf2);
						p2_actions[p2_action_counter] = ACTION_BLOCK;
						++p2_action_counter;
					}

					if (!strcmp("c", buf2))
					{
						printf("Server: P2 CHARGE\n", buf2);
						p2_actions[p2_action_counter] = ACTION_CHARGE;
						++p2_action_counter;
					}

					if (p2_action_counter >= 3)
					{
						p2_ready = 1;
						printf("Server: P2 READY\n", buf2);
						if (p1_ready)
						{
							printf("Server: BOTH READY. EXECUTING ACTIONS!\n", buf2);
							gamestate = GAMESTATE_EXECUTING;
							countdown = 3;
							p1_action_counter = 0;
							p2_action_counter = 0;
							p1_ready = 0;
							p2_ready = 0;
						}
					}
				}
			}

			if (gamestate == GAMESTATE_GAMEOVER)
			{
				if (!strcmp("r", buf2))
				{
					if (client_count == 2)
					{
						++replay_count;
						printf("Server: Player 2 wants to play again\n");
					}
					else
					{
						send(accepted_clients[1], "em", 3, 0);
						serverNeutralize();
					}
				}
			}

			buf2[msg2] = 0;
		}
	}

	printf("Server: Stopped listening Client 2\n");
	Thread_join(&client2_listener);

	return 0;
}

void checkPlayerStatus()
{
	if (p1_position <= 0 || p2_position >= 20 || p1_hp <= 0 || p2_hp <= 0)
	{
		gamestate = GAMESTATE_GAMEOVER;
		printf("Server: GAME OVER!\n");
	}

	if (p1_position <= 0 || p1_hp <= 0)
	{
		send(accepted_clients[0], "d1", 3, 0);
		send(accepted_clients[1], "d1", 3, 0);
		printf("Server: P1 DEAD!\n");
	}

	if (p2_position >= 20 || p2_hp <= 0)
	{
		send(accepted_clients[0], "d2", 3, 0);
		send(accepted_clients[1], "d2", 3, 0);
		printf("Server: P2 DEAD!\n");
	}


}

void matchReset()
{
	replay_count = 0;

	countdown = 3;
	gamestate = GAMESTATE_COUNTDOWN;

	p1_ready = 0;
	p2_ready = 0;

	p1_charge = 0;
	p2_charge = 0;

	p1_position = 8;
	p2_position = 12;

	p1_hp = 100;
	p2_hp = 100;

	p1_action_counter = 0;
	p2_action_counter = 0;

	for (int i = 0; i < 3; ++i)
	{
		p1_actions[i] = ACTION_PUNCH;
		p2_actions[i] = ACTION_PUNCH;
	}
}
