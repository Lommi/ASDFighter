
#pragma once

#include <stdio.h>
#include <time.h>
#include <math.h>

#include "texture.h"
#include "game.h"

#define inline __inline

bool mainInit();

void mainClose();

SDL_Window		*window;
SDL_Renderer	*renderer;

GameSession		game;
