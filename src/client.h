#pragma once

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>

#include "sys_win32.h"
#include "game.h"

#pragma comment (lib, "Ws2_32.lib")

#define PORT "3490" // the port client will be connecting to 

#define MAXDATASIZE 64 // max number of bytes we can get at once 

typedef struct GameSession GameSession;

int sockfd;
int client_running;

GameSession *client_session_ptr;

void *get_in_addr(struct sockaddr *sa);

void clientShutdown(int id);

void clientSetSession(GameSession *gs);

thread_ret_t clientStart(void*);