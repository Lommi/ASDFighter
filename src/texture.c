#include "texture.h"

void textureFree(SDL_Texture *texture)
{
	//Free texture if it exists
	if (texture != 0)
	{
		SDL_DestroyTexture(texture);
		texture = 0;
	}
}

bool textureLoad(SDL_Renderer *renderer, Texture *texture, char *path)
{
	bool success = 0;

	textureFree(texture->texture);

	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path);
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path, IMG_GetError());
	}
	else
	{
		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
		if (newTexture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", path, SDL_GetError());
		}
		else
		{
			texture->texture = newTexture;
			texture->width = loadedSurface->w;
			texture->height = loadedSurface->h;
			texture->clips[0].x = 0;
			texture->clips[0].y = 0;
		}
		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	return success;
}

void textureRender(SDL_Renderer *renderer, Texture *texture, int x, int y, SDL_Rect *clip, int flip)
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, texture->width, texture->height };

	//Set clip rendering dimensions
	if (clip != 0)
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen
	SDL_RenderCopyEx(renderer, texture->texture, clip, &renderQuad, 0, 0, flip);
}