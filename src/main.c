#include "main.h"

const int SCREEN_WIDTH = 480;
const int SCREEN_HEIGHT = 200;

bool mainInit()
{
	window = 0;
	renderer = 0;

	bool success = true;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Warning: Linear texture filtering not enabled!");
		}

		window = SDL_CreateWindow("", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

		if (window == 0)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (renderer == 0)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

void
mainClose()
{
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	window = 0;
	renderer = 0;

	clientShutdown(sockfd);
	Thread_join(&client_thread);
	
	IMG_Quit();
	SDL_Quit();
}

int main(int argc, char* args[])
{
	if (!mainInit())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		gameInit(&game, renderer);

		bool quit = false;
		SDL_Event e;

		while (!quit)
		{
			while (SDL_PollEvent(&e) != 0)
			{
				if (e.type == SDL_QUIT)
				{
					quit = true;
				}
				if (e.type == SDL_KEYDOWN)
				{
					switch (e.key.keysym.sym)
					{
						case SDLK_ESCAPE:
						{
							quit = true;
						}break;
					}
				}
			}

			gameUpdate(&game);

			SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
			SDL_RenderClear(renderer);

			gameRender(&game, renderer);

			SDL_RenderPresent(renderer);
		}
	}

	mainClose();
    
	return 0;
}