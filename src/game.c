#include "game.h"

int player_id;

void gameInit(GameSession *session, SDL_Renderer *renderer)
{
	clientSetSession(session);

	textureLoad(renderer, &session->texture_player, "../assets/characters.png");
	textureLoad(renderer, &session->texture_titles, "../assets/titles.png");
    textureLoad(renderer, &session->texture_hp_border, "../assets/hp_border.png");
    textureLoad(renderer, &session->texture_hp_fill, "../assets/hp_fill.png");
    textureLoad(renderer, &session->texture_background, "../assets/background.png");
    textureLoad(renderer, &session->texture_border, "../assets/border.png");
	textureLoad(renderer, &session->texture_gameover_text, "../assets/gameover_text.png");
	textureLoad(renderer, &session->texture_actions, "../assets/action_icons.png");

    textureSetClip(&session->texture_hp_fill, 0, 0, 0, 100, 16);
    textureSetClip(&session->texture_hp_fill, 1, 0, 16, 100, 16);

	textureSetClip(&session->texture_actions, 0, 0, 0, 16, 16);
	textureSetClip(&session->texture_actions, 1, 17, 0, 16, 16);
	textureSetClip(&session->texture_actions, 2, 33, 0, 16, 16);

	textureSetClip(&session->texture_gameover_text, 0, 0, 0, 168, 58);
	textureSetClip(&session->texture_gameover_text, 1, 0, 60, 168, 58);
	textureSetClip(&session->texture_gameover_text, 2, 0, 118, 168, 58);

    session->gamestate = GAMESTATE_MENU;

	session->player[0].texture = session->texture_player;

	session->player[1].texture = session->texture_player;

    session->effect.texture = session->texture_player;

    session->hpbar[0].texture = session->texture_hp_fill;
    session->hpbar[0].x = 44;
    session->hpbar[0].y = 14;
	session->hpbar[0].player_actions[0] = 4;
	session->hpbar[0].player_actions[1] = 4;
	session->hpbar[0].player_actions[2] = 4;
	session->hpbar[0].action_icons_texture = session->texture_actions;

    session->hpbar[1].texture = session->texture_hp_fill;
    session->hpbar[1].x = 334;
    session->hpbar[1].y = 14;
	session->hpbar[1].player_actions[0] = 4;
	session->hpbar[1].player_actions[1] = 4;
	session->hpbar[1].player_actions[2] = 4;
	session->hpbar[1].action_icons_texture = session->texture_actions;
	
    textureSetClip(&session->texture_titles, 0, 30, 15, 450, 30);
    textureSetClip(&session->texture_titles, 1, 30, 60, 450, 30);
    textureSetClip(&session->texture_titles, 2, 30, 100, 450, 30);
    textureSetClip(&session->texture_titles, 3, 30, 136, 450, 30);
    textureSetClip(&session->texture_titles, 4, 30, 169, 450, 30);
    textureSetClip(&session->texture_titles, 5, 30, 212, 450, 30);
    textureSetClip(&session->texture_titles, 6, 30, 255, 450, 30);

    textureSetClip(&session->texture_background, 0, 31, 0, 357, 48);
    textureSetClip(&session->texture_background, 1, 458, 74, 480, 200);

    gamePlay(session);
}

void gamePlay(GameSession *session)
{
    playerSetState(session, &session->player[0], STATE_IDLE);
    session->player[0].x = PLAYER1_DEFAULT_X_POSITION;
    session->player[0].y = PLAYER1_DEFAULT_Y_POSITION;
    session->player[0].hp = 100;
    session->player[0].falling = 0;
	session->player[0].position_counter = 8;
	session->player[0].hpbar = &session->hpbar[0];
	session->player[0].hit_dir = 1;
	session->player[0].block_dir = -1;
	session->player[0].action_counter = 0;
	session->player[0].charge = 0;

    playerSetState(session, &session->player[1], STATE_IDLE);
    session->player[1].x = PLAYER2_DEFAULT_X_POSITION;
    session->player[1].y = PLAYER1_DEFAULT_Y_POSITION;
    session->player[1].hp = 100;
    session->player[1].falling = 0;
	session->player[1].position_counter = 12;
	session->player[1].hpbar = &session->hpbar[1];
	session->player[1].hit_dir = -1;
	session->player[1].block_dir = 1;
	session->player[1].action_counter = 0;
	session->player[1].charge = 0;

	session->hpbar[0].player_actions[0] = 4;
	session->hpbar[0].player_actions[1] = 4;
	session->hpbar[0].player_actions[2] = 4;

	session->hpbar[1].player_actions[0] = 4;
	session->hpbar[1].player_actions[1] = 4;
	session->hpbar[1].player_actions[2] = 4;

    textureSetClip(&session->hpbar[0].texture, 0, 0, 0, session->player[0].hp, 16);
    textureSetClip(&session->hpbar[1].texture, 0, 0, 0, session->player[1].hp, 16);

    if (session->gamestate != GAMESTATE_MENU)
        session->gamestate = GAMESTATE_ACTIVE;

    session->countdown = 3;
    session->countdown_timer = 0;

    session->effect.visible = 0;
}

void gameUpdate(GameSession *session)
{
	const Uint8* currentKeyStates = SDL_GetKeyboardState(0);

	switch (session->gamestate)
	{
		case GAMESTATE_MENU:
		{
			if (currentKeyStates[SDL_SCANCODE_RETURN])
			{
				Thread_create(&client_thread, clientStart, 0);
				session->gamestate = GAMESTATE_CONNECTING_TO_SERVER;
			}
		}
		break;

		case GAMESTATE_ACTIVE:
		case GAMESTATE_EXECUTING:
        case GAMESTATE_GAMEOVER:
        {
            if (session->player[0].state == STATE_DEAD)
            {
                if (!session->player[0].falling)
                    session->player[0].x -= 8;
                else
                {
                    session->player[0].y += 8;
                }
            }
            if (session->player[1].state == STATE_DEAD)
            {
                if (!session->player[1].falling)
                    session->player[1].x += 8;
                else
                {
                    session->player[1].y += 8;
                }
            }

			if (currentKeyStates[SDL_SCANCODE_RETURN] && session->gamestate == GAMESTATE_GAMEOVER)
			{
				send(sockfd, "r", 3, 0);
				session->gamestate = GAMESTATE_WAITING_OTHER_CLIENTS;
			}

            ++session->update_frames;

            if (session->update_frames > TIMER_TICK)
            {
                playerUpdate(session, &session->player[0]);
                playerUpdate(session, &session->player[1]);
                if (session->effect.visible)
                    effectUpdate(session);

                session->update_frames = 0;
            }

            if (session->countdown == 0 && session->gamestate == GAMESTATE_ACTIVE)
            {
                if (currentKeyStates[SDL_SCANCODE_D])
                {
                    if (!d_pressed)
					{
						if (!d_pressed && !a_pressed && !s_pressed)
						{
							send(sockfd, "p", 3, 0);

							session->player[player_id].hpbar->player_actions[session->player[player_id].action_counter] = 0;
							++session->player[player_id].action_counter;
						}
                    }
                    d_pressed = 1;
                }
                else
                    d_pressed = 0;

                if (currentKeyStates[SDL_SCANCODE_S])
                {
					if (!s_pressed)
					{
						if (!d_pressed && !a_pressed && !s_pressed)
						{
							send(sockfd, "b", 3, 0);

							session->player[player_id].hpbar->player_actions[session->player[player_id].action_counter] = 1;
							++session->player[player_id].action_counter;
						}
					}
                    s_pressed = 1;
                }
                else
                {
                    s_pressed = 0;
                }
				if (currentKeyStates[SDL_SCANCODE_A])
				{
					if (!a_pressed)
					{
						if (!d_pressed && !a_pressed && !s_pressed)
						{
							send(sockfd, "c", 3, 0);

							session->player[player_id].hpbar->player_actions[session->player[player_id].action_counter] = 2;
							++session->player[player_id].action_counter;
						}
					}
					a_pressed = 1;
				}
				else
				{
					a_pressed = 0;
				}

				if (session->player[player_id].action_counter >= 3)
				{
					session->gamestate = GAMESTATE_EXECUTING;
					session->player[player_id].action_counter = 0;
				}
            }
            break;
        }
	}
}

void gameRender(GameSession *session, SDL_Renderer* renderer)
{
	switch (session->gamestate)
	{
		case GAMESTATE_MENU:
		case GAMESTATE_CONNECTING_TO_SERVER:
		case GAMESTATE_WAITING_OTHER_CLIENTS:
		{
            textureRender(renderer, &session->texture_background, 0, 0, &session->texture_background.clips[1], 0);
            textureRender(renderer, &session->texture_background, 64, 160, &session->texture_background.clips[0], 0);
			if(session->gamestate == GAMESTATE_MENU)
				textureRender(renderer, &session->texture_titles, 30, 60, &session->texture_titles.clips[0], 0);
			if (session->gamestate == GAMESTATE_CONNECTING_TO_SERVER)
				textureRender(renderer, &session->texture_titles, 30, 60, &session->texture_titles.clips[1], 0);
			if (session->gamestate == GAMESTATE_WAITING_OTHER_CLIENTS)
				textureRender(renderer, &session->texture_titles, 30, 60, &session->texture_titles.clips[3], 0);
		}
		break;

		case GAMESTATE_EXECUTING:
		case GAMESTATE_ACTIVE:
        case GAMESTATE_GAMEOVER:
		{
            textureRender(renderer, &session->texture_background, 0, 0, &session->texture_background.clips[1], 0);
            textureRender(renderer, &session->texture_background, 64, 160, &session->texture_background.clips[0], 0);

            if(session->countdown > 0)
                textureRender(renderer, &session->texture_titles, 40, 60, &session->texture_titles.clips[7 - session->countdown], 0);

			textureRender(renderer, &session->player[0].texture, session->player[0].x,
				session->player[0].y, &session->player[0].texture.clips[session->player[0].current_frame], 0);

			textureRender(renderer, &session->player[1].texture, session->player[1].x,
				session->player[1].y, &session->player[1].texture.clips[session->player[1].current_frame], 1);

            if (session->effect.visible)
                textureRender(renderer, &session->effect.texture, session->effect.x,
                    session->effect.y, &session->effect.texture.clips[session->effect.current_frame], 0);

            textureRender(renderer, &session->hpbar[0].texture, session->hpbar[0].x, session->hpbar[0].y, &session->hpbar[0].texture.clips[1], 0);
            textureRender(renderer, &session->hpbar[1].texture, session->hpbar[1].x, session->hpbar[1].y, &session->hpbar[1].texture.clips[1], 0);
            textureRender(renderer, &session->hpbar[0].texture, session->hpbar[0].x, session->hpbar[0].y, &session->hpbar[0].texture.clips[0], 0);
            textureRender(renderer, &session->hpbar[1].texture, session->hpbar[1].x, session->hpbar[1].y, &session->hpbar[1].texture.clips[0], 0);

			textureRender(renderer, &session->hpbar[0].action_icons_texture, session->hpbar[0].x, session->hpbar[0].y + 32, &session->hpbar[0].action_icons_texture.clips[session->hpbar[0].player_actions[0]], 0);
			textureRender(renderer, &session->hpbar[0].action_icons_texture, session->hpbar[0].x + 20, session->hpbar[0].y + 32, &session->hpbar[0].action_icons_texture.clips[session->hpbar[0].player_actions[1]], 0);
			textureRender(renderer, &session->hpbar[0].action_icons_texture, session->hpbar[0].x + 40, session->hpbar[0].y + 32, &session->hpbar[0].action_icons_texture.clips[session->hpbar[0].player_actions[2]], 0);

			textureRender(renderer, &session->hpbar[1].action_icons_texture, session->hpbar[1].x, session->hpbar[1].y + 32, &session->hpbar[1].action_icons_texture.clips[session->hpbar[1].player_actions[0]], 0);
			textureRender(renderer, &session->hpbar[1].action_icons_texture, session->hpbar[1].x + 20, session->hpbar[1].y + 32, &session->hpbar[1].action_icons_texture.clips[session->hpbar[1].player_actions[1]], 0);
			textureRender(renderer, &session->hpbar[1].action_icons_texture, session->hpbar[1].x + 40, session->hpbar[1].y + 32, &session->hpbar[1].action_icons_texture.clips[session->hpbar[1].player_actions[2]], 0);

            textureRender(renderer, &session->texture_hp_border, 30, 4, 0, 0);
            textureRender(renderer, &session->texture_hp_border, 320, 4, 0, 0);

			if (session->player[0].state == STATE_DEAD && session->player[1].state != STATE_DEAD)
			{
				if (player_id == 0)
					textureRender(renderer, &session->texture_gameover_text, 150, 60, &session->texture_gameover_text.clips[1], 0);
				else
					textureRender(renderer, &session->texture_gameover_text, 150, 60, &session->texture_gameover_text.clips[0], 0);
			}
			else if (session->player[1].state == STATE_DEAD && session->player[0].state != STATE_DEAD)
			{
				if (player_id == 1)
					textureRender(renderer, &session->texture_gameover_text, 150, 60, &session->texture_gameover_text.clips[1], 0);
				else
					textureRender(renderer, &session->texture_gameover_text, 150, 60, &session->texture_gameover_text.clips[0], 0);
			}
			else if (session->player[0].state == STATE_DEAD && session->player[1].state == STATE_DEAD)
				textureRender(renderer, &session->texture_gameover_text, 150, 60, &session->texture_gameover_text.clips[2], 0);
		}
		break;
	}
}

void playerSetState(GameSession *session, Player *player, int state)
{
    player->current_frame = 0;

	switch (state)
	{
		case STATE_IDLE:
		{
			player->state = STATE_IDLE;

            textureSetClip(&player->texture, 0, 1, 1, 45, 45);
            textureSetClip(&player->texture, 1, 47, 1, 45, 45);
            textureSetClip(&player->texture, 2, 93, 1, 45, 45);
		}
		break;

		case STATE_PUNCH:
        {
			player->hpbar->player_actions[player->action_counter] = 0;
			++player->action_counter;

            player->state = STATE_PUNCH;
			player->x += KNOCKBACK_DISTANCE * player->hit_dir;
			player->position_counter += 1 * player->hit_dir;

            switch (player->punch_counter)
            {
                case 0:
                {
					effectShow(session, EFFECT_HIT_SMALL, player->x + 21, player->y - 3);

                    textureSetClip(&player->texture, 0, 1, 47, 45, 45);
                    textureSetClip(&player->texture, 1, 1, 47, 45, 45);
                    textureSetClip(&player->texture, 2, 47, 47, 45, 45);
                }
                break;

                case 1:
                {
					effectShow(session, EFFECT_HIT_SMALL, player->x + 21, player->y - 3);

                    textureSetClip(&player->texture, 0, 93, 47, 45, 45);
                    textureSetClip(&player->texture, 1, 93, 47, 45, 45);
                    textureSetClip(&player->texture, 2, 139, 47, 45, 45);
                }
                break;

                case 2:
                {
					effectShow(session, EFFECT_HIT_LARGE, player->x + 21, player->y - 3);

                    textureSetClip(&player->texture, 0, 231, 47, 45, 45);
                    textureSetClip(&player->texture, 1, 231, 47, 45, 45);
                    textureSetClip(&player->texture, 2, 185, 47, 45, 45);
                }
                break;
            }

            ++player->punch_counter;

            if (player->punch_counter > 2)
                player->punch_counter = 0;
        }
		break;

		case STATE_BLOCK:
		{
			player->hpbar->player_actions[player->action_counter] = 1;
			++player->action_counter;

			player->state = STATE_BLOCK;

			player->x += KNOCKBACK_DISTANCE * player->block_dir;
			player->position_counter += 1 * player->block_dir;

			player->punch_counter = 0;

			effectShow(session, EFFECT_BLOCK, player->x + 20, player->y);

            textureSetClip(&player->texture, 0, 1, 93, 45, 45);
            textureSetClip(&player->texture, 1, 1, 93, 45, 45);
            textureSetClip(&player->texture, 2, 47, 93, 45, 45);

		}
		break;

		case STATE_CHARGE:
		{
			player->hpbar->player_actions[player->action_counter] = 2;
			++player->action_counter;

			player->state = STATE_CHARGE;

			player->punch_counter = 0;
			//effectShow(session, EFFECT_BLOCK, player->x + 20, player->y);

			textureSetClip(&player->texture, 0, 1, 93, 45, 45);
			textureSetClip(&player->texture, 1, 1, 93, 45, 45);
			textureSetClip(&player->texture, 2, 47, 93, 45, 45);

		}
		break;

		case STATE_HURT:
		{
			player->state = STATE_HURT;

            textureSetClip(&player->texture, 0, 1, 139, 45, 45);
            textureSetClip(&player->texture, 1, 1, 139, 45, 45);
            textureSetClip(&player->texture, 2, 1, 139, 45, 45);
		}
		break;

        case STATE_DEAD:
        {
            player->state = STATE_DEAD;

            textureSetClip(&player->texture, 0, 47, 139, 45, 45);
            textureSetClip(&player->texture, 1, 47, 139, 45, 45);
            textureSetClip(&player->texture, 2, 47, 139, 45, 45);
        }
        break;
	}
}

void playerUpdate(GameSession *session, Player *player)
{
	if (player->state != STATE_DEAD)
	{
        ++player->current_frame;

		if (player->current_frame > 2)
		{
            playerSetState(session, player, STATE_IDLE);
		}
	}
}

void playerTakeDamage(GameSession *session, Player *player, int amount, int position_amount)
{
	player->hp -= amount;
	player->x += (KNOCKBACK_DISTANCE * position_amount) * player->block_dir;
	player->position_counter += position_amount * player->block_dir;

	playerSetState(session, player, STATE_HURT);
	textureSetClip(&player->hpbar->texture, 0, 0, 0, player->hp, 16);
}

void effectShow(GameSession *session, int type, int x, int y)
{
    session->effect.visible = 1;
    session->effect.current_frame = 0;
    session->effect.x = x;
    session->effect.y = y;
    
    switch (type)
    {
        case EFFECT_HIT_SMALL:
        {
            session->effect.frame_count = 3;
            textureSetClip(&session->effect.texture, 0, 93, 202, 45, 45);
            textureSetClip(&session->effect.texture, 1, 139, 202, 45, 45);
            textureSetClip(&session->effect.texture, 2, 185, 202, 45, 45);
        }
        break;
        case EFFECT_HIT_LARGE:
        {
            session->effect.frame_count = 3;
            textureSetClip(&session->effect.texture, 0, 93, 295, 45, 44);
            textureSetClip(&session->effect.texture, 1, 139, 295, 45, 44);
            textureSetClip(&session->effect.texture, 2, 185, 295, 45, 44);
        }
        break;
        case EFFECT_BLOCK:
        {
            session->effect.frame_count = 3;
            textureSetClip(&session->effect.texture, 0, 47, 248, 45, 45);
            textureSetClip(&session->effect.texture, 1, 93, 248, 45, 45);
            textureSetClip(&session->effect.texture, 2, 139, 248, 45, 45);
        }
        break;
    }
}

void effectUpdate(GameSession *session)
{
    ++session->effect.current_frame;
    if (session->effect.current_frame > session->effect.frame_count)
    {
        session->effect.visible = 0;
    }
}