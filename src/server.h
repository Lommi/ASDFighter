#pragma once

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <fcntl.h>
#include <SQL\sqlite3.c>

#include "sys_win32.h"

#pragma comment (lib, "Ws2_32.lib")

#define inline __inline

#define PORT "3490"  // the port users will be connecting to
#define BACKLOG 3     // how many pending connections queue will hold
#define DEFAULT_BUFLEN 512
#define MAXDATASIZE 64 // max number of bytes we can get at once 

enum Player_actions
{
	ACTION_PUNCH = 0,
	ACTION_BLOCK,
	ACTION_CHARGE
};

enum GameState
{
	GAMESTATE_NONE = 0,
	GAMESTATE_COUNTDOWN,
	GAMESTATE_TAKING_INPUT,
	GAMESTATE_EXECUTING,
	GAMESTATE_GAMEOVER
};

Thread client1_listener, client2_listener;

int server_running, gamestate, acceptloop_running;
int accepted_clients[2];
int client_count;
int replay_count;
int msg1, msg2;

int p1_action_counter, p2_action_counter;
int p1_actions[3];
int p2_actions[3];
int p1_ready, p2_ready;

int p1_position, p2_position;
int p1_hp, p2_hp;
int p1_charge, p2_charge;

int start_tick;

int countdown;

char buf1[MAXDATASIZE];
char buf2[MAXDATASIZE];

static int callback(void *data, int argc, char **argv, char **azColName);

void *get_server_addr(struct sockaddr *sa);
void serverNeutralize();
void serverShutdown();

void checkPlayerStatus();
void matchReset();

thread_ret_t serverHearClient1(void*);
thread_ret_t serverHearClient2(void*);

sqlite3 *db;
sqlite3_stmt *stmt;
char *errmsg = NULL;
int rc;
int logins;

char sql_string[20] = "some string";
char client1_ip[20] = "some string";
char client2_ip[20] = "some string";
char* query;
const char* data = "Callback function called";
int rows = 0;

int ban_player(char *address);
void sql_check();