#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_rect.h>
#include <stdio.h>
#include <stdbool.h>

#define inline __inline

typedef struct Texture	Texture;

void textureFree(SDL_Texture *texture);

bool textureLoad(SDL_Renderer* renderer, Texture *texture, char *path);

void textureRender(SDL_Renderer* renderer, Texture *texture, int x, int y, SDL_Rect *clip, int flip);

static inline void textureSetClip(Texture *texture, int clip_index, int x, int y, int w, int h);

struct Texture
{
	SDL_Texture *texture;
	SDL_Rect	clips[7];

	int width;
	int height;
};

static inline void textureSetClip(Texture *texture, int clip_index, int x, int y, int w, int h)
{
	texture->clips[clip_index].x = x;
	texture->clips[clip_index].y = y;
	texture->clips[clip_index].w = w;
	texture->clips[clip_index].h = h;
}